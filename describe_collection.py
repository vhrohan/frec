# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)

import boto3
from botocore.exceptions import ClientError
session = boto3.Session()
# Any clients created from this session will use credentials
# from the [dev] section of ~/.aws/credentials.
client = session.client('rekognition')


def describe_collection_aws(collection_id):

    print('Attempting to describe collection ' + collection_id)

    try:
        response = client.describe_collection(CollectionId=collection_id)
        print("Collection Arn: " + response['CollectionARN'])
        print("Face Count: " + str(response['FaceCount']))
        print("Face Model Version: " + response['FaceModelVersion'])
        print("Timestamp: " + str(response['CreationTimestamp']))
        return response

    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print('The collection ' + collection_id + ' was not found ')
            return {"status": False, "message": 'The collection ' + collection_id + ' was not found '}
        else:
            print('Error other than Not Found occurred: ' +
                  e.response['Error']['Message'])
            return {"status": False, "message": e.response['Error']['Message']}

# def main():
#    collection_id = 'Collection'
#    describe_collection(collection_id)


# if __name__ == "__main__":
#    main()
