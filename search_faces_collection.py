# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)
import psycopg2
import urllib.request
import boto3
from db_cursor import getDbConnCursor
session = boto3.Session()
# Any clients created from this session will use credentials
client = session.client('rekognition')
connection = getDbConnCursor()


def search_face_in_collection_face_id(face_id, collection_id):
    threshold = 90
    max_faces = 10
    response = client.search_faces(CollectionId=collection_id,
                                   FaceId=face_id,
                                   FaceMatchThreshold=threshold,
                                   MaxFaces=max_faces)

    face_matches = response['FaceMatches']
    print('Matching faces')
    for match in face_matches:
        print('FaceId:' + match['Face']['FaceId'])
        print('Similarity: ' + "{:.2f}".format(match['Similarity']) + "%")
        print
    return len(face_matches)


def detect_text(imgPath):
    detectedText = []

    response = client.detect_text(Image={
        'Bytes': urllib.request.urlopen(imgPath).read()},)

    textDetections = response['TextDetections']
    print('Detected text\n----------')
    for text in textDetections:
        tempText = {}
        if text['Type'] == 'LINE' and (text['Confidence']*1) > 90:
            tempText['DetectedText'] = text['DetectedText']
            tempText['Confidence'] = text['Confidence']
            tempText['Type'] = text['Type']
            detectedText.append(tempText)
            # print('Detected text:' + text['DetectedText'])
            # print('Confidence: ' + "{:.2f}".format(text['Confidence']) + "%")
            # print('Id: {}'.format(text['Id']))
            # if 'ParentId' in text:
            #     print('Parent Id: {}'.format(text['ParentId']))
            # print('Type:' + text['Type'])
            # print()
    return detectedText


def search_face_in_collection_img(imgPath, collectionId):
    threshold = 70
    maxFaces = 2
    matches = []
    response = client.search_faces_by_image(CollectionId=collectionId,
                                            Image={
                                                'Bytes': urllib.request.urlopen(imgPath).read()},
                                            FaceMatchThreshold=threshold,
                                            MaxFaces=maxFaces)

    faceMatches = response['FaceMatches']
    print('Matching faces')
    for match in faceMatches:
        matchTemp = []
        try:
            cursor = connection.cursor()
        except (Exception, psycopg2.InterfaceError) as e:
            print("creating db conn object")
            cursor = connection.cursor()
        try:
            select_query = 'select face_id from public.faceaws where aws_faceid=%s;'
            cursor.execute(select_query, (str(match['Face']['FaceId']),))
            face_id = cursor.fetchone()
            face_id = face_id if face_id is None else face_id[0]
            if(face_id is None):
                raise Exception("No faceid with this aws_faceid")
            # matchTemp.append(match['Face']['FaceId'])
            matchTemp.append(face_id)
            matchTemp.append("{:.4f}".format(match['Similarity']/100))
            print('Old FaceId:' + face_id)
            print('AWS FaceId:' + match['Face']['FaceId'])
            print('Similarity: ' + "{:.2f}".format(match['Similarity']) + "%")
            matches.append(matchTemp)
        except Exception as err:
            print(err)
        cursor.close()
    return matches


# def main():
#     # face_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
#     collection_id = 'Collection'

#     # faces_count = search_face_in_collection_face_id(face_id, collection_id)
#     faces_count = search_face_in_collection_img(
#         '/Users/rohan/Documents/KSP-IPH-2019-TABLE03-master/dataset/mine/Ronny.jpg', collection_id)
#     print("faces found: " + str(faces_count))


# if __name__ == "__main__":
#     main()
