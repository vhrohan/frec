from add_face_collection import add_face_to_collection, insert_faceid_to_db
from concurrent.futures import ProcessPoolExecutor, wait, as_completed


def migrate_faces():
    f = open('badchacterdata.csv', 'r')
    count = 0
    for i in f.readlines():
        try:
            if(count > 5468):
                rec = [str(j).strip() for j in i.split(',')]
                face_id = add_face_to_collection(rec[-1], 'canadasalman_cctns')
                insert_faceid_to_db(
                    rec[-2], face_id, rec[-1], 'canadasalman_cctns')
                print(count, rec[-1], rec[-2])
        except Exception as e:
            print("failed for ", rec[-1], str(e))
        count += 1


if __name__ == '__main__':
    migrate_faces()
