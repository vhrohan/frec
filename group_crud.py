import psycopg2
from db_cursor import getDbConnCursor


connection = getDbConnCursor()


def create_group(name: str, description: str = ""):
    global connection
    try:
        existing_group = None
        if(name is not None and len(name) > 3):
            columns = ['group_id', 'name', 'description']
            try:
                cursor = connection.cursor()
            except (Exception, psycopg2.InterfaceError) as e:
                print("creating db conn object")
                connection.close()
                connection = getDbConnCursor()
                cursor = connection.cursor()
            select_query = 'select ' + \
                ','.join(columns)+' from public.group where name=%s'
            cursor.execute(select_query, (name,))
            existing_group = cursor.fetchone()
            group_id = None
            if(existing_group is None):
                insert_query = 'insert into public.group(name, description) values(%s,%s) RETURNING group_id;'
                cursor.execute(insert_query, (name, description))
                group_id = cursor.fetchone()[0]
                connection.commit()
            else:
                group_id = existing_group[0]
            return {"status": "success", "body": {"group_id": group_id, "name": name, "description": description}}
        else:
            raise Exception("group name should be more than 3 letters!!")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if(name is not None and len(name) > 3 and existing_group is None):
            connection.rollback()
        cursor.close()
        return {"status": "failed", "error": str(error)}


def delete_group(name: str) -> bool:
    global connection
    try:
        if(name is not None and len(name) > 0):
            try:
                cursor = connection.cursor()
            except (Exception, psycopg2.InterfaceError) as e:
                print("creating db conn object")
                connection.close()
                connection = getDbConnCursor()
                cursor = connection.cursor()
            delete_query = 'delete from public.group where name=%s'
            cursor.execute(delete_query, (name,))
            connection.commit()
            cursor.close()
            return {"status": "success", "body": "deleted successfully"}
        else:
            raise Exception("No group name given..")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if(name is not None and len(name) > 0):
            connection.rollback()
        cursor.close()
        return {"status": "failed", "error": str(error)}


def list_view_group():
    global connection
    try:
        try:
            cursor = connection.cursor()
        except (Exception, psycopg2.InterfaceError) as e:
            print("creating db conn object")
            connection.close()
            connection = getDbConnCursor()
            cursor = connection.cursor()
        columns = ['name', 'group_id', 'description']
        select_query = 'select ' + ','.join(columns)+' from public.group;'
        cursor.execute(select_query, ())
        data = []
        for i in cursor.fetchall():
            entry = {}
            for c in range(len(columns)):
                entry[columns[c]] = i[c]
            data.append(entry)
        cursor.close()
        return {"status": "success", "body": data}
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return {"status": "failed", "error": str(error)}


def view_group(name: str):
    global connection
    try:
        if(name is not None and len(name) > 0):
            try:
                cursor = connection.cursor()
            except (Exception, psycopg2.InterfaceError) as e:
                print("creating db conn object")
                connection.close()
                connection = getDbConnCursor()
                cursor = connection.cursor()
            columns = ['group_id', 'name', 'description']
            select_query = 'select ' + \
                ','.join(columns)+' from public.group where name=%s;'
            cursor.execute(select_query, (name,))
            data = cursor.fetchone()
            if(data is not None):
                entry = {}
                for c in range(len(columns)):
                    entry[columns[c]] = data[c]
                cursor.close()
                return {"status": "success", "body": [entry]}
            else:
                raise Exception("No group found with specified name..")
        else:
            raise Exception("please provide group name")
    except (Exception, psycopg2.DatabaseError) as error:
        if(name is not None and len(name) > 0):
            cursor.close()
        print(error)
        return {"status": "failed", "body": str(error)}
