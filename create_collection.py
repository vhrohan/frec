# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)

import boto3
from botocore.exceptions import ClientError
session = boto3.Session()
# Any clients created from this session will use credentials
# from the [dev] section of ~/.aws/credentials.
client = session.client('rekognition')


def create_collection_aws(collection_id):
    try:
        # Create a collection
        print('Creating collection:' + collection_id)
        response = client.create_collection(CollectionId=collection_id)
        print('Collection ARN: ' + response['CollectionArn'])
        print('Status code: ' + str(response['StatusCode']))
        print('Done...')
        return response
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceAlreadyExistsException':
            print('The collection ' + collection_id + ' already exists ')
            return {"status": False, "message": 'The collection ' + collection_id + ' already exists '}
        else:
            print('Error other than Not Found occurred: ' +
                  e.response['Error']['Message'])
            return {"status": False, "message": e.response['Error']['Message']}


# def main():
#    collection_id = 'empty'
#    create_collection_aws(collection_id)


# if __name__ == "__main__":
#    main()
