# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)

from db_cursor import getDbConnCursor
# import boto3
# import os
# import psycopg2
# import urllib.request
# session = boto3.Session(profile_name='rezzler')
import urllib.request
import boto3
import os
import psycopg2
session = boto3.Session()
# Any clients created from this session will use credentials
client = session.client('rekognition')
connection = getDbConnCursor()


def add_faces_to_collection(photos, collection_id):
    faces = [False]*len(photos)
    for i in range(len(photos)):
        if(i == 1000):
            return faces
        try:
            #f = open(photos[i], "rb")
            image_url = ""
            response = client.index_faces(CollectionId=collection_id,
                                          Image={
                                              'Bytes': urllib.request.urlopen(image_url)},
                                          MaxFaces=1,
                                          QualityFilter="AUTO",
                                          DetectionAttributes=['ALL'])

            faces[i] = len(response['FaceRecords']
                           ) > 0 if response['FaceRecords'] else False
        except Exception as e:
            print("Error for ", photos[i], str(e))
            continue
    return faces


def add_face_to_collection(photo, collection_id):
    face_id = None
    try:

        response = client.index_faces(CollectionId=collection_id,
                                      Image={
                                          'Bytes': urllib.request.urlopen(photo).read()},
                                      MaxFaces=1,
                                      QualityFilter="AUTO",
                                      DetectionAttributes=['ALL'])

        if len(response['FaceRecords']) > 0:
            face_id = response['FaceRecords'][0]['Face']['FaceId']
            result = insert_faceid_to_db(
                face_id, face_id, photo, collection_id)
            if(result['status'] != "success"):
                raise Exception("No face detected")

    except Exception as e:
        print("Error for ", photo, str(e))

    return {"status": "success", "body": {"face_id": face_id, "group_id": collection_id}}


def insert_faceid_to_db(faceid, aws_faceid, image_url, group_name):
    global connection
    try:
        group_id = None
        embed = None
        try:
            cursor = connection.cursor()
        except (Exception, psycopg2.InterfaceError) as e:
            print("creating db conn object")
            connection.close()
            connection = getDbConnCursor()
            cursor = connection.cursor()

        if(aws_faceid is not None):
            insert_query = 'insert into public.faceaws(face_id, image_url, aws_faceid, group_name) values (%s,%s,%s,%s)'
            cursor.execute(
                insert_query, (faceid, image_url, aws_faceid, group_name))
            connection.commit()
            cursor.close()
            return {"status": "success", "body": {"face_id": faceid, "group_id": group_id}}
        else:
            raise Exception("No face detected")

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if(group_id is not None and embed is not None):
            connection.rollback()
        cursor.close()
        return {"status": "failed", "error": str(error)}


def gen_embs_from_folder(img_path, img_type="jpg"):
    images = []
    for file in os.listdir(img_path):
        if file.endswith("."+img_type):
            curr_img_path = os.path.join(img_path, file)
            images.append(curr_img_path)
    return images


def main():
    bucket = 'subahuface'
    collection_id = 'Collection'
    photos = gen_embs_from_folder(
        '/Users/rohan/Documents/KSP-IPH-2019-TABLE03-master/dataset/2017_data_collected_photos')
    # print(photos[:150])
    indexed_faces_count = add_face_to_collection(
        photos, collection_id)
    print("Faces indexed count: " + str(indexed_faces_count))


if __name__ == "__main__":
    main()
