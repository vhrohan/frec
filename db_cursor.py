import psycopg2


def getDbConnCursor(user="postgres", passwd="admin123", host="frecdb.c8ixqycs64n4.us-east-1.rds.amazonaws.com", port="5432", database="postgres"):

    connection = None
    try:
        connection = psycopg2.connect(user=user, password=passwd,
                                      host=host, port=port, database=database)
    except Exception as err:
        print("Can't connect ", err)
    return connection
