from flask import Flask, json, request
from group_crud import create_group, delete_group, view_group, list_view_group

from add_face_collection import add_face_to_collection
from search_faces_collection import search_face_in_collection_img
from search_faces_collection import detect_text
from create_collection import create_collection_aws
from describe_collection import describe_collection_aws
from delete_collection import delete_collection_aws
from db_cursor import getDbConnCursor
app = Flask(__name__)


@app.route('/', methods=['GET'])
def health_check():
    response = app.response_class(response=json.dumps(
        {"healthy": True, "message": "Flask app is running fine..."}), mimetype='application/json')
    return response


@app.route('/create_group', methods=['POST'])
def register_group():
    req = request.json
    if(req is not None):
        response = app.response_class(response=json.dumps(
            create_collection_aws(req.get('name'))), mimetype='application/json')
        return response


@app.route('/list_group', methods=['GET'])
def list_group():
    response = app.response_class(response=json.dumps(
        list_view_group()), mimetype='application/json')
    return response


@app.route('/get_group', methods=['POST'])
def get_group():
    req = request.json
    if(req is not None):
        response = app.response_class(response=json.dumps(
            describe_collection_aws(str(req.get('name')))), mimetype='application/json')
        return response


@app.route('/delete_group', methods=['POST'])
def remove_group():
    req = request.json
    if(req is not None):
        response = app.response_class(response=json.dumps(
            delete_collection_aws(str(req.get('name')))), mimetype='application/json')
        return response


@app.route('/register_face', methods=['POST'])
def register_face():
    req = request.json
    responseData = {}
    try:
        if(req is None):
            raise Exception("Invalid request payload")
        if(req.get('image_url') is None or req.get('group_name') is None):
            raise Exception("provide image url and group id")
        responseData = add_face_to_collection(
            req.get('image_url'), req.get('group_name'))
        if responseData['status'] == 'success':
            responseData['message'] = "New Face Registered"
    except Exception as err:
        responseData['status'] = 'failed'
        responseData['error'] = str(err)
    response = app.response_class(response=json.dumps(
        responseData), mimetype='application/json')
    return response


@app.route('/match_faces', methods=['POST'])
def match_faces_api():
    result = {}
    req = request.json
    matched_faces = []
    if(req is None):
        result['status'] = 'failed'
        result['message'] = "Invalid request payload, provide both image_url and group_name"
        return app.response_class(response=json.dumps(
            result), mimetype='application/json')
    if(req.get('image_url') is None or req.get('group_name') is None):
        result['status'] = 'failed'
        result['message'] = "provide both image_url and group_name"
    else:
        count = 5 if req.get('count') is None else int(req.get('count'))
        # matched_details = match_faces(req.get('image_url'), req.get('group_name'), count, 0.5)
        try:
            matched_details = search_face_in_collection_img(
                req.get('image_url'), req.get('group_name'))
        except Exception as e:
            return app.response_class(response=json.dumps(
                {"status": "failed", "error": str(e)}), mimetype='application/json')

        if matched_details is not None:
            for element in matched_details:
                temp_dict = {}
                temp_dict['face_id'] = element[0]
                temp_dict['score'] = element[1]
                matched_faces.append(temp_dict)
            result['status'] = 'success'
            result['message'] = "Matches Found"
            result['body'] = {
                'group_name': req.get('group_name'), 'matches': matched_faces}
        else:
            result['status'] = 'failed'
            result['message'] = "No Matches Found"
            result['body'] = {}
    response = app.response_class(response=json.dumps(
        result), mimetype='application/json')
    return response


@app.route('/detect_text', methods=['POST'])
def detect_text_api():
    result = {}
    req = request.json
    matched_faces = []
    if(req is None):
        result['status'] = 'failed'
        result['message'] = "Invalid request payload, provide both image_url and group_name"
        return app.response_class(response=json.dumps(
            result), mimetype='application/json')
    else:
        try:
            matched_details = detect_text(req.get('image_url'))
        except Exception as e:
            return app.response_class(response=json.dumps(
                {"status": "failed", "error": str(e)}), mimetype='application/json')

        if matched_details is not None:
            result['status'] = 'success'
            result['message'] = "Matches Found"
            result['body'] = {'Text': matched_details}
        else:
            result['status'] = 'failed'
            result['message'] = "No Text Found"
            result['body'] = {}
    response = app.response_class(response=json.dumps(
        result), mimetype='application/json')
    return response


if __name__ == '__main__':
    from waitress import serve
    serve(app, host="0.0.0.0", port=80)
