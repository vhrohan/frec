import face_recognition
import os
import psycopg2
import numpy as np
import time
import urllib.request
import uuid
from db_cursor import getDbConnCursor
connection = getDbConnCursor()


def gen_embs_from_folder(img_path, img_type="jpg"):
    embeds = []
    embed_name = []
    err = []
    for file in os.listdir(img_path):
        if file.endswith("."+img_type):
            curr_img_path = os.path.join(img_path, file)
            image = face_recognition.load_image_file(curr_img_path)
            # Find all the faces in the image using the default HOG-based model.
            # This method is fairly accurate, but not as accurate as the CNN model and not GPU accelerated.
            # See also: find_faces_in_picture_cnn.py
            try:
                enc = face_recognition.face_encodings(
                    image)[0]  # .reshape(1,-1)
            except IndexError:
                err.append(file)
                continue
            embeds.append(enc)
            embed_name.append(file)
    return embeds, embed_name, err


def gen_embs_from_db(group_name):
    global connection
    try:
        cursor = connection.cursor()
    except (Exception, psycopg2.InterfaceError) as e:
        print("creating db conn object")
        connection.close()
        connection = getDbConnCursor()
        cursor = connection.cursor()
    embeds = []
    embed_ids = []
    try:
        select_query = 'select group_id from public.group where name=%s;'
        cursor.execute(select_query, (str(group_name),))
        group_id = cursor.fetchone()
        group_id = group_id if group_id is None else group_id[0]
        if(group_id is None):
            raise Exception("No group with this name")
        select_query = 'select * from public.face where group_id=%s'
        cursor.execute(select_query, (group_id,))
        for i in cursor.fetchall():
            embeds.append(np.loads(i[2]))
            embed_ids.append(i[1])
    except Exception as err:
        print(err)
    cursor.close()
    return embeds, embed_ids


def gen_emb_from_url(image_url):
    try:
        image = face_recognition.load_image_file(
            urllib.request.urlopen(image_url))
        enc = face_recognition.face_encodings(
            image, num_jitters=50)[0]
        return enc
    except Exception as error:
        print(error)
        return None


def insert_embed_to_db(image_url, group_name):
    global connection
    try:
        group_id = None
        embed = None
        try:
            cursor = connection.cursor()
        except (Exception, psycopg2.InterfaceError) as e:
            print("creating db conn object")
            connection.close()
            connection = getDbConnCursor()
            cursor = connection.cursor()
        select_query = 'select group_id from public.group where name=%s;'
        cursor.execute(select_query, (str(group_name),))
        group_id = cursor.fetchone()
        group_id = group_id if group_id is None else group_id[0]
        if(group_id is None):
            raise Exception("No group with this name")
        embed = gen_emb_from_url(image_url)
        if(embed is not None):
            image_id = uuid.uuid1().hex
            insert_query = 'insert into public.face(face_id, image_encoding, group_id, photo_angle, image_url) values (%s,%s,%s,%s,%s)'
            cursor.execute(insert_query, (image_id, psycopg2.Binary(
                embed.dumps()), group_id, 'center', image_url))
            connection.commit()
            cursor.close()
            return {"status": "success", "body": {"face_id": image_id, "group_id": group_id}}
        else:
            raise Exception("No face detected")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        if(group_id is not None and embed is not None):
            connection.rollback()
        cursor.close()
        return {"status": "failed", "error": str(error)}


def migrate_embed_to_db(url, image_id):
    global connection
    try:
        cursor = connection.cursor()
    except (Exception, psycopg2.InterfaceError) as e:
        print(str(e), "creating db conn object")
        connection.close()
        connection = getDbConnCursor()
        cursor = connection.cursor()
    embed = gen_emb_from_url(url)
    try:
        if(url is None or image_id is None):
            raise("provide url and id")
        if(embed is not None):
            update_query = 'update public.face set image_encoding = %s, image_url=%s where face_id=%s'
            cursor.execute(update_query, (psycopg2.Binary(
                embed.dumps()), url, image_id))
            connection.commit()
            cursor.close()
            return 'Success'
        else:
            raise Exception("No face detected")
    except Exception as e:
        print(str(e))
        return None


12: 24


Message Rohan


Downloads
